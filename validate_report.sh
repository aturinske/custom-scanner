#!/bin/bash

# Get the schema files
curl -s https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/archive/master/security-report-schemas-master.zip?path=src > schemas.zip

# Extract the schema files
mkdir schemas 2> /dev/null # Quiet mode
unzip -q -o -j -d schemas schemas.zip

# Build the command to update the file reference
JQ_COMMAND=".allOf[0].\"\$ref\"=\""
JQ_COMMAND=${JQ_COMMAND}file://`pwd`/schemas/security-report-format.json\"
#echo $JQ_COMMAND

# Save the update schema with our paths
jq $JQ_COMMAND < schemas/dast-report-format.json > updated-schema.json

# Validate the report against the schema
jsonschema -i $1 updated-schema.json
