This project shows an example of integrating a custom security scanner inside
of GitLab. It produces results which are then displayed in the various parts
of the GitLab UI.

# Overall workflow

This repo runs a custom security scanner, called `SamScan`, as part of a CI
pipeline whenever a new commit is made. `SamScan` writes its results to a file
which GitLab:reads and processes. The CI in this project also runs a step to
validate that the results of `SamScan` are valid and what GitLab expects.

# Validating report results

GitLab has published schemas on what we expect custom scanners to produce. It
is a best practice to validate your products output against these schemas so
that you do not experience errors or mis-displayed data from your results.

The [`validate_report.sh`](validate_report.sh) script shows an example of how
this might be done. This script is run as part of this example project in the
`validate_schema` job inside of [`.gitlab-ci.yml`](.gitlab-ci.yml). You should
at least do this testing and validation while building your integration.
