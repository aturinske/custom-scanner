import json

# This is the output of the scanner that will be parsed by GitLab.
report = {
  "version": "2.2",
  "vulnerabilities": [
    {
      "category": "A category",
      "name": "Sam's custom vulnerability",
      "message": "There is a vulnerability... somewhere.",
      "description": "Let's make a long-form description to explain the problem and why it is a vulnerability",
      "cve": "Another unique fingerprint value - asdnmvxzaslk391011",
      "severity": "High",
      "scanner": {
        "id": "my_custom_scanner",
        "name": "A Custom Scanner"
      },
      "location": {
        "file": "src/myapp.c",
        "start_line": 8,
        "dependency": {
          "package": {}
        }
      },
      "identifiers": [
        {
          "type": "my_scan_id",
          "name": "MyScan ID: 17",
          "value": "17"
        }
      ]
    },
    {
      "category": "A category",
      "name": "Sam's second custom vulnerability",
      "message": "There is ANOTHER vulnerability... somewhere.",
      "description": "Let's make a long-form description to explain the problem and why it is a vulnerability",
      "cve": "A unique fingerprint value - klsnmllfgg298390rp1fli2ifh",
      "severity": "Medium",
      "scanner": {
        "id": "my_custom_scanner",
        "name": "A Custom Scanner"
      },
      "location": {
        "file": "src/myapp.c",
        "start_line": 20,
        "dependency": {
          "package": {}
        }
      },
      "identifiers": [
        {
          "type": "my_scan_id",
          "name": "MyScan ID: 18",
          "value": "18"
        }
      ]
    }
  ],
  "remediations": []
}

# Print a JSON string of the report above
print(json.dumps(report))

